export class Subject {
	subjectId: number;
	subjectTitle: string;
	durationInHours: number;
}
