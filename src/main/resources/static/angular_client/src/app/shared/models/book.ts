export class Book {
	bookId: number;
	price: number;
	publishDate: string;
	title: string;
	volume:number;
	subjectId: number;
}
