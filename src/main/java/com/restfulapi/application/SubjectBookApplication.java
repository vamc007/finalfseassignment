package com.restfulapi.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubjectBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubjectBookApplication.class, args);
	}
}



