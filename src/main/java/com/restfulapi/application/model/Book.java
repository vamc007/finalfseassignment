package com.restfulapi.application.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name="book")
public class Book{

	@Id
	@Column(name="book_id")
	private Integer bookId;	
	
	@Column(name="price")
	private Double price;
	
	@Column(name="publishdate")
	private Date publishDate;
	
	@Column(name="title")	
	private String title;	

	@Column(name="volume")
	private Integer volume;	
	
	//we will create 1 transient field for subject id
	private transient Long subjectId;
	
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.REFRESH,CascadeType.DETACH,CascadeType.REMOVE})
	@JoinColumn(name = "subjectid")
	@JsonIgnoreProperties("references")
	private Subject subject;

	public Book() {
		super();
	}	
	
	public Book(Integer bookId, Double price, Date publishDate, String title, Integer volume, Subject subject) {
		super();
		this.bookId = bookId;
		this.price = price;
		this.publishDate = publishDate;
		this.title = title;
		this.volume = volume;
		this.subject = subject;
	}


	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}
	
	public Long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	}
	
	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookId == null) ? 0 : bookId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (bookId == null) {
			if (other.bookId != null)
				return false;
		} else if (!bookId.equals(other.bookId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", price=" + price + ", publishDate=" + publishDate + ", title=" + title
				+ ", volume=" + volume + ", subjectId=" + subjectId + "]";
	}	
}
